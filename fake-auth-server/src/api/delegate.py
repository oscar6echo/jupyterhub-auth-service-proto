
import os
import base64

import datetime as dt
import requests as rq

from flask import request
from flask_restful import Resource

from .config import CLIENT_ID, CLIENT_SECRET, TOKEN_DURATION
from .util import Util

# os.environ['current_access_token'] = 'init'


class DelegateApi(Resource):
    """
    """

    def post(self):
        """
        provides new token
        """
        data = Util.parse_form(request.form)
        print(data)

        auth_header = request.headers.get('Authorization')
        print(auth_header)
        login_pwd = auth_header.split(' ')[1].encode('utf-8')
        login_pwd = base64.decodebytes(login_pwd).decode('utf-8')
        client_id, client_secret = login_pwd.split(':')
        print(client_id, client_secret)

        if client_id != CLIENT_ID:
            output = {'description': 'wrong client id'}
            status_code = 451
            return output, status_code

        if client_secret != CLIENT_SECRET:
            output = {'description': 'wrong client secret'}
            status_code = 452
            return output, status_code

        print(os.environ['current_access_token'])
        print(data.get('access_token'))
        if data.get('access_token') != os.environ['current_access_token']:
            output = {'description': 'wrong refresh token'}
            status_code = 453
            return output, status_code

        now = dt.datetime.now()
        tag = now.strftime('%H%M%S')
        access_token = 'access-token-'+tag
        iat = now
        exp = now + dt.timedelta(seconds=TOKEN_DURATION)
        refresh_token = 'refresh-token-'+tag
        scope = data.get('scope')

        os.environ['current_access_token'] = access_token
        os.environ['current_refresh_token'] = refresh_token

        iat = iat.isoformat()
        exp = exp.isoformat()

        status_code = 200
        output = {
            'access_token': access_token,
            'scope': scope,
            'iat': iat,
            'exp': exp,
            'refresh_token': refresh_token
        }
        print(output)
        return output, status_code
