
import os

import datetime as dt
import requests as rq

from flask import request
from flask_restful import Resource

from .config import CLIENT_ID, CLIENT_SECRET, INIT_REFRESH_TOKEN, TOKEN_DURATION
from .util import Util

os.environ['current_refresh_token'] = 'init'


class TokenApi(Resource):
    """
    """

    def post(self):
        """
        provides new token
        """
        data = Util.parse_data(request.data)
        print(data)

        if data.get('client_id') != CLIENT_ID:
            output = {'description': 'wrong client id'}
            status_code = 451
            return output, status_code

        if data.get('client_secret') != CLIENT_SECRET:
            output = {'description': 'wrong client secret'}
            status_code = 452
            return output, status_code

        last_refresh_token = os.environ['current_refresh_token']
        if last_refresh_token != 'init':
            if data.get('refresh_token') != last_refresh_token:
                output = {'description': 'wrong refresh token'}
                status_code = 453
                return output, status_code

        now = dt.datetime.now()
        tag = now.strftime('%H%M%S')
        access_token = 'access-token-'+tag
        iat = now
        exp = now + dt.timedelta(seconds=TOKEN_DURATION)
        refresh_token = 'refresh-token-'+tag
        scope = 'JupyterHub openid profile'

        os.environ['current_access_token'] = access_token
        os.environ['current_refresh_token'] = refresh_token

        iat = iat.isoformat()
        exp = exp.isoformat()

        status_code = 200
        output = {
            'access_token': access_token,
            'scope': scope,
            'iat': iat,
            'exp': exp,
            'refresh_token': refresh_token
        }
        print(output)
        return output, status_code
