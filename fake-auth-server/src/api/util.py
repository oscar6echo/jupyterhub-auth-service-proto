
from urllib.parse import parse_qsl


class Util:
    """
    """
    def __init__(self):
        """
        """
        pass

    @staticmethod
    def parse_data(raw):
        """
        """
        qs = raw.decode('utf-8')
        data = dict(parse_qsl(qs))
        return data


    @staticmethod
    def parse_form(form):
        """
        """
        dic = form.to_dict(flat=True)
        return dic


