from flask import Flask
from flask_restful import Api

app = Flask(__name__)
app.config['DEBUG'] = True

# create api
_api = Api(app)

# api endpoints
from .api.token import TokenApi
_api.add_resource(TokenApi, '/token', methods=['POST'], endpoint='token')

from .api.delegate import DelegateApi
_api.add_resource(DelegateApi, '/delegate', methods=['POST'], endpoint='delegate')
