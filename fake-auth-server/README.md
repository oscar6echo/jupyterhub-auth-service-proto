
# Fake Authentication Server

For demo purpose.  

> **IMPORTANT**: The fake auth server must be launched after the jupyter notebook server.

To launch:

```bash
$ source activate hubauth
(hubauth) $ python run.py
```
