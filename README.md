
# JupyterHub Auth Service Prototype

## 1 - Overview

This repo contains a Python package, [`hub_auth`](./hub-auth), to manage the auth token from the notebook.  
It consists in a client package and a server extension.  


## 1 - Run

Install the `hub_auth` package and run the demo notebook:

```bash
# from this repo root

# create env hubauth - for reproducibility
$ conda create -n hubauth python=3
$ source activate hubauth
$ pip install -r requirement.txt
$ source activate hubauth

# install hub_auth
(hubauth) $ cd hub_auth
(hubauth) $ pip install . 
(hubauth) $ pip install -e . # else for dev install

# enable server extension
$ jupyter serverextension enable --py hub_auth.hub_auth_server

# add env to kernel list
$ python -m ipykernel install --user --name hubauth

# launch jupyter
(hubauth) $ jupyter notebook
```

Run the [fake auth server](./fake-auth-server) from another terminal:

```bash
$ source activate hubauth
(hubauth) $ cd fake-auth-server
# launch server AFTER jupyter notebook server
(hubauth) $ python run.py
```
