
import json

import datetime as dt

from copy import deepcopy as copy


DATETIME_ISOFORMAT = '%Y-%m-%dT%H:%M:%S.%f'


class Util:
    """
    """

    def __init__(self):
        """
        """
        pass

    @staticmethod
    def json_dumps(token_data):
        """
        like json.dumps with date special case
        """
        # print('in json_dumps')
        # print(token_data)

        data = copy(token_data)
        for k in ['iat', 'exp']:
            if k in data:
                data[k] = data[k].isoformat()
        return json.dumps(data)

    @staticmethod
    def json_loads(token_data):
        """
        like json.loads with date special case
        """
        # print('in json_loads')
        # print(token_data)

        data = json.loads(token_data)
        for k in ['iat', 'exp']:
            if k in data:
                data[k] = dt.datetime.strptime(data[k], DATETIME_ISOFORMAT)
        return data

