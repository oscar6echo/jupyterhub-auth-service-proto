
import json

import datetime as dt
import requests as rq

from .util import Util


class HubAuth:
    """
    """

    def __init__(self, host='localhost',
                 port=8888,
                 buffer_in_seconds=3,
                 raise_for_error=True):
        """
        """
        self.host = host
        self.port = port
        self.buffer_in_seconds = buffer_in_seconds
        self.raise_for_error = raise_for_error

        self.base_url = 'http://{}:{}'.format(host, port)
        self.endpoint = '{}/token'.format(self.base_url)
        self.access_token = {}
        self.cookies = {}

        self.get_cookie()

    def get_token(self,
                  refresh=False,
                  scope=None,
                  include_granted_scopes=True,
                  verbose=False):
        """
        """
        if refresh or scope:
            if include_granted_scopes:
                current_scope = self.access_token.get('scope', [])
                new_scope = set(current_scope.split(' ') + scope.split(' '))
                new_scope = ' '.join(list(new_scope))
            else:
                new_scope = scope
            self.renew(force_refresh=True,
                       scope=new_scope,
                       verbose=verbose)

        if not self.is_token_valid():
            self.renew(force_refresh=False,
                       verbose=verbose)

        return self.access_token

    def get_value(self,
                  verbose=False):
        """
        """
        access_token = self.get_token(verbose=verbose)
        return access_token.get('access_token')

    def renew(self,
              force_refresh=False,
              scope=None,
              verbose=False):
        """
        """
        url = self.endpoint
        cookies = self.cookies
        headers = {}
        data = {'force_refresh': force_refresh,
                '_xsrf': self.cookies['_xsrf']}
        if scope:
            data['scope'] = scope
        if verbose:
            print('renew token: request')
            print('\tcookies =', cookies)
            print('\tdata =', data)
        r = rq.post(url,
                    cookies=cookies,
                    headers=headers,
                    data=data)
        try:
            content = r.content
            if verbose:
                print('renew token: response')
                print('\tcontent =', content)
            content_str = content.decode('utf-8')
            res = Util.json_loads(content_str)
            if 'error' in res:
                self.error()
            else:
                self.access_token = res
        except:
            self.error()

    def is_token_valid(self):
        """
        """
        exp = self.access_token.get('exp')

        if not exp:
            return False

        now = dt.datetime.now()
        if now > exp - dt.timedelta(seconds=self.buffer_in_seconds):
            return False

        return True

    def get_cookie(self):
        """
        """
        url = 'http://{}:{}/tree/'.format(self.host, self.port)
        r = rq.get(url)
        self.cookies = {'_xsrf': r.cookies['_xsrf']}

    def error(self):
        """
        """
        if self.raise_for_error:
            raise Exception('HubAuth error: Cannot retrieve access token')
        else:
            self.access_token = {}
