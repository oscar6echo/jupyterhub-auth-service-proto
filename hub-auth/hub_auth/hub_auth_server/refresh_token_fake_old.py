

async def refresh_token_fake(client_id,
                             client_secret,
                             refresh_token):
    """
    Fetch new token from fake authorization server
    """

    # login_pwd = '{}:{}'.format(CLIENT_ID, CLIENT_SECRET)
    # b64key = base64.encodebytes(login_pwd.encode('utf-8')).decode('utf-8')

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
    body = urlencode({
        'client_id': CLIENT_ID,
        'client_secret': CLIENT_SECRET,
        'refresh_token': os.getenv('refresh_token'),
        'grant_type': 'refresh_token'
    })

    req = HTTPRequest(URL_OAUTH2_TOKEN,
                      method='POST',
                      headers=headers,
                      body=body,
                      #   auth_username=CLIENT_ID,
                      #   auth_password=CLIENT_SECRET
                      )

    client = AsyncHTTPClient()
    res = await client.fetch(req)

    res_json = json.loads(res.body.decode('utf8'))

    if is_valid_res(res_json):
        store_token_data()
        return True
    else:
        return False
        clear_token_data()
