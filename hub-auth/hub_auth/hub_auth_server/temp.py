params = dict(
    grant_type='authorization_code',
    redirect_uri=redirect_uri,
    code=code
)

url = self.endpoint + '/oauth2/access_token'

headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'User-Agent': USER_AGENT_NAME,
    'Authorization': 'Basic {}'.format(b64key.decode("utf8"))
}


req = requests.post(url,
                    headers=headers,
                    # data=urlencode(params))
                    data=params)
