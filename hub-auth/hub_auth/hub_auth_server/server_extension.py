
import os
import json
# import base64

import datetime as dt

from copy import deepcopy as copy

from urllib.parse import urlencode

# from tornado import web
# from tornado.ioloop import IOLoop
from tornado.ioloop import PeriodicCallback
# from tornado.httpclient import AsyncHTTPClient, HTTPRequest

from notebook.utils import url_path_join
from notebook.base.handlers import IPythonHandler

from .config import CLIENT_ID, CLIENT_SECRET
from .util import Util
from .fetch_token_refresh_fake import fetch_token_refresh_fake
from .fetch_token_delegate_fake import fetch_token_delegate_fake


def is_valid_current_token():
    """
    returns True is current token in env var is valid
    """
    token_data = Util.load_token_data()
    if Util.is_valid(token_data):
        return True
    return False


async def fetch_token_refresh(pc=None):
    """
    Fetch new token from authorization server
    using refresh token
    and update cache
    """

    print('in fetch_token_refresh')

    # if Util.is_valid(token_data):
    #     print('no need to refresh')
    #     return

    refresh_token = Util.load_refresh_token()
    # print('>>>>>>>>>>>>> refresh_token')
    # print(refresh_token)
    token_data = await fetch_token_refresh_fake(client_id=CLIENT_ID,
                                                client_secret=CLIENT_SECRET,
                                                refresh_token=refresh_token)

    if Util.is_valid(token_data):
        print('in refresh_token_cache: token is valid')
        period = Util.compute_period(token_data)
        if pc:
            print('******** reset callback_time')
            pc.stop()
            pc.callback_time = 1e3 * period
            pc.start()
        Util.store_token_data(token_data)

    else:
        Util.clear_token_data()


async def fetch_token_delegate(scope, pc=None):
    """
    Fetch new token from authorization server
    using delegate endpoint
    and update cache
    """

    print('in fetch_token_delegate')

    token_data = Util.load_token_data()
    access_token = token_data.get('access_token', None)

    # print('>>>>>>>>>>>>> refresh_token')
    # print(refresh_token)
    token_data = await fetch_token_delegate_fake(client_id=CLIENT_ID,
                                                 client_secret=CLIENT_SECRET,
                                                 access_token=access_token,
                                                 scope=scope)

    if Util.is_valid(token_data):
        print('in refresh_token_cache: token is valid')
        period = Util.compute_period(token_data)
        if pc:
            print('******** reset callback_time')
            pc.stop()
            pc.callback_time = 1e3 * period
            pc.start()
        Util.store_token_data(token_data)

    else:
        Util.clear_token_data()


def load_jupyter_server_extension(nb_app):
    """
    Call upon loading extension
    """

    web_app = nb_app.web_app

    Util.create_token_data_at_startup()
    period = Util.compute_period()

    print('period =', period)

    # loop = IOLoop.current()
    pc = PeriodicCallback(fetch_token_refresh, 1e3 * period)
    pc.start()

    class TokenHandler(IPythonHandler):
        """
        Client requests new token, potentially with new scopes
        """

        # @web.authenticated
        # def get(self):
        #     """
        #     """
        #     print('TokenHandler GET')
        #     self.write(json.dumps({'toto': 22}))

        async def post(self):
            """
            """
            print('TokenHandler POST')
            force_refresh = self.get_argument('force_refresh')
            force_refresh = force_refresh == 'True'
            scope = self.get_argument('scope', default=None)
            nb_app.log.info('in TokenHandler force_refresh={} scope={}'.format(force_refresh,
                                                                               scope))

            print('scope =', scope)

            if force_refresh or not is_valid_current_token():
                await fetch_token_refresh(pc)

            if scope:
                await fetch_token_delegate(scope, pc)

            token_data = Util.load_token_data()
            print(token_data)

            self.write(Util.json_dumps(token_data))

    host_pattern = '.*$'
    base_url = web_app.settings['base_url']

    web_app.add_handlers(
        host_pattern,
        [
            (url_path_join(base_url, '/token'), TokenHandler),
        ]
    )

    nb_app.log.info('hub-auth server extension enabled')
