
import json

from urllib.parse import urlencode

from tornado.httpclient import AsyncHTTPClient, HTTPRequest


from .util import Util


async def fetch_token_refresh_fake(client_id,
                                   client_secret,
                                   refresh_token):
    """
    Fetch new token from fake authorization server
    """

    url = 'http://localhost:5000/token'
    headers = {
        # 'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
    body = urlencode({
        'client_id': client_id,
        'client_secret': client_secret,
        'refresh_token': refresh_token,
        'grant_type': 'refresh_token'
    })

    req = HTTPRequest(url,
                      method='POST',
                      headers=headers,
                      body=body,
                      )

    client = AsyncHTTPClient()
    res = await client.fetch(req, raise_error=False)

    print('-------START RESPONSE REFRESH')

    if res.error:
        print('Error: {}'.format(res.error))
        dic = {'error': res.code}

    else:
        print(res.body)
        dic = Util.json_loads(res.body.decode('utf8'))

    print(dic)
    print('-------END RESPONSE REFRESH')

    return dic
