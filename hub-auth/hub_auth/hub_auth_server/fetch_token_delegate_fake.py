
import json
import base64

from urllib.parse import urlencode

from tornado.httpclient import AsyncHTTPClient, HTTPRequest


from .util import Util


async def fetch_token_delegate_fake(client_id,
                                    client_secret,
                                    access_token,
                                    scope):
    """
    Fetch new token from fake authorization server
    """

    scope = scope.split(' ')+['JupyterHub', 'openid', 'profile']
    scope = ' '.join(list(set(scope)))

    key = '{}:{}'.format(client_id, client_secret)
    b64key = base64.encodebytes(key.encode('utf-8')).decode('utf-8')
    print(b64key)

    url = 'http://localhost:5000/delegate'
    headers = {
        # 'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        # 'Authorization': 'Basic {}'.format(b64key)
    }
    body = urlencode({
        'access_token': access_token,
        'scope': scope,
    })

    req = HTTPRequest(url,
                      method='POST',
                      headers=headers,
                      auth_username=client_id,
                      auth_password=client_secret,
                      body=body,
                      )

    client = AsyncHTTPClient()
    res = await client.fetch(req, raise_error=False)

    print('-------START RESPONSE DELEGATE')

    if res.error:
        print('Error: {}'.format(res.error))
        dic = {'error': res.code}

    else:
        print(res.body)
        dic = Util.json_loads(res.body.decode('utf8'))

    print(dic)
    print('-------END RESPONSE DELEGATE')

    return dic
