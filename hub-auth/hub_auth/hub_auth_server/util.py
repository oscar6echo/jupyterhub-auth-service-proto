
import os
import json

import datetime as dt

from copy import deepcopy as copy


DATETIME_ISOFORMAT = '%Y-%m-%dT%H:%M:%S.%f'
TOKEN_DURATION = 5
DELAY_SECONDS = 1


class Util:
    """
    """

    def __init__(self):
        """
        """
        pass

    @staticmethod
    def is_valid(token_data):
        """
        return True if access token data is valid
        """
        print('in is_valid')
        print(token_data)

        if not token_data:
            print('outcome 0')
            return False

        access_token = token_data.get('access_token')
        scope = token_data.get('scope')
        iat = token_data.get('iat')
        exp = token_data.get('exp')

        if ((access_token is None) or
                (scope is None) or
                (iat is None) or
                (exp is None)):
            print('outcome 1')
            return False

        now = dt.datetime.now()
        if now > exp - dt.timedelta(seconds=DELAY_SECONDS):
            print('outcome 2')
            return False

        print('outcome 3')
        return True

    @staticmethod
    def json_dumps(token_data):
        """
        like json.dumps with date special case
        """
        print('in json_dumps')
        print(token_data)

        data = copy(token_data)
        for k in ['iat', 'exp']:
            if k in data:
                data[k] = data[k].isoformat()
        return json.dumps(data)

    @staticmethod
    def store_token_data(token_data):
        """
        store token_data in env var
        """
        print('in store_token_data')
        print(token_data)

        data = copy(token_data)

        refresh_token = data.pop('refresh_token', None)
        if refresh_token and len(refresh_token):
            print('save refresh_token')
            os.environ['refresh_token'] = refresh_token

        print(data)
        data = Util.json_dumps(data)
        os.environ['token_data'] = data

    @staticmethod
    def json_loads(token_data):
        """
        like json.loads with date special case
        """
        print('in json_loads')
        print(token_data)

        data = json.loads(token_data)
        for k in ['iat', 'exp']:
            if k in data:
                data[k] = dt.datetime.strptime(data[k], DATETIME_ISOFORMAT)
        return data

    @staticmethod
    def load_token_data():
        """
        load token_data from env var
        """
        print('in load_token_data')

        token_data = os.environ.get('token_data')
        if token_data is None:
            return {'error': 'no access token'}
        data = Util.json_loads(token_data)
        print(data)
        return data

    @staticmethod
    def load_refresh_token():
        """
        load refresh_token from env var
        """
        print('in load_refresh_token')

        data = os.environ.get('refresh_token')
        print(data)
        return data

    @staticmethod
    def clear_token_data():
        """
        clear token_data env var
        """
        print('in clear_token_data')

        if os.environ.get('token_data'):
            del os.environ['token_data']

    # @staticmethod
    # def clear_refresh_token():
    #     """
    #     clear refresh_token env var
    #     """
    #     print('in clear_refresh_token')

    #     if os.environ.get('refresh_token'):
    #         del os.environ['refresh_token']

    @staticmethod
    def create_token_data_at_startup():
        """
        Fake init
        """
        print('in create_token_data_at_startup')

        now = dt.datetime.now()
        d = {
            'token': 'init-access-token',
            'scope': 'scopeA scopeB',
            'iat': now,
            'exp': now + dt.timedelta(seconds=TOKEN_DURATION),
        }
        Util.store_token_data(d)
        os.environ['refresh_token'] = 'init-refresh-token'

    @staticmethod
    def compute_period(data=None):
        """
        return time in sec between current exp and iat
        """
        if not data:
            data = Util.load_token_data()
        if data:
            diff = (data['exp']-data['iat']).seconds - DELAY_SECONDS
            return diff
        return 0
