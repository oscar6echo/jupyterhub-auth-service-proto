
# HubAuth Python package

## 1 - Overview

The `hub_auth` Python package has 2 parts:
+ `hub_auth_client`
+ `hub_auth_server`

The `hub_auth_client` used inside the notebook, provides a convenient access to the auth token obtained and managed (refresh, delegation) by the server.

The `hub_auth_server` is the corresponding server extension. It performs 2 operations:
+ Token refresh: Replace a token before it reaches its maturity date.
+ Token delegation: Exchange a token containing a given set of scopes for another containing those plus a new set of scopes requested by the user through the `hub_auth_client`.  

To provide these services the server extension communicates with the authorisation server (fake here). Naturally only the services provided by the auth server can be offered to the client. The refresh is standard, the delegation is not.



## 2 - Context

This is a prototype for a JupyterHub service.  
The `hub_auth_client` can remain unchanged, but could also be completely removed if the JupyterHub server has the ability to update an env var any time.  
The `hub_auth_server` must be carved and pasted into a JupyterHub service.  


## 3 - Install

To install the package:

```bash
# from folder hub-auth/

# install package
$ pip install . # regular install
$ pip install -e . # dev install i.e. source symlinked

# enable server extension
$ jupyter serverextension enable --py hub_auth.hub_auth_server
```

